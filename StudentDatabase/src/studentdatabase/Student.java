
package studentdatabase;


public class Student {
    
    String firstName;
    String sureName;
    String middleName;
    int course;
    String group;

    @Override
    public String toString() {
        return "firstName=" + firstName + ", sureName=" + sureName + ", middleName=" + middleName + ", course=" + course + ", group=" + group + '}';
    }
    
    public String  print(String message)
    {
        return "Сообщение для студента ["+sureName+"] ==> "+ message;
    }     
    
    
    
}
