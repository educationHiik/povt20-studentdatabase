
package studentdatabase;


public class StudentDatabase {

    
    public static void main(String[] args) {
        
        System.out.println("База данных студентов");
        Student student1 = new Student();
        Student student2 = new Student();
        
        student1.firstName = "Иван";
        student1.sureName = "Иванов";
        student1.middleName = "Иванович";
        student1.course = 1;
        student1.group = "ПОВТ-20Д";
        
        System.out.println(student1.toString());
        
        String message = student1.print("Привет студент");
        System.out.println(message);
       
        
    }
    
}
